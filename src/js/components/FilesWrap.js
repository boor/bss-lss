/**
 * Created by alexboor on 3/2/2017.
 */

import React, {Component} from 'react'

const classNames = require('classnames');

const DOC_TYPES = {
    '0': '-- Укажите --',
    '1': 'Пасспорт',
    '2': 'ОГРН',
    '3': 'СНИЛС'
};


export default class FilesWrap extends Component {

    constructor() {
        super();

        this.state = {
            is_active: null,
            list: {}
        }
    }

    componentDidMount() {
        this.updatePageArray();
    }

    componentDidUpdate() {
        this.updatePageArray();
    }


    /**
     * updatePageArray
     *
     * Update page index row when from files pages data
     */
    updatePageArray() {
        for (let f in this.props.files) {
            if (!(f in this.state.list)) {
                if (Object.keys(this.props.files[f].pages).length) {
                    this.setState({
                        list: {
                            ...this.state.list,
                            [f]: Object.keys(this.props.files[f].pages)
                        }
                    });
                }
            }
        }
    }

    /**
     * getPageNum
     *
     * Return page number
     *
     * @param filename {String} Filename
     * @param page_id {String} page id
     * @returns {Number}
     */
    getPageNum(filename, page_id) {
        if (filename && page_id) {
            if (Object.keys(this.state.list).length) {
                return +this.state.list[filename].indexOf(page_id) + 1;
            }
        return 0;
        }
    }

    /**
     * setPageNum
     *
     * Set new page number for defined page in defined filename
     *
     * @param filename {String} Filename
     * @param page_id {String} page id
     * @param num {Number} new page number
     */
    setPageNum(filename, page_id, num) {
        num--;

        if (filename && page_id) {
            let ex_index = this.state.list[filename].indexOf(page_id);
            let ex_page = this.state.list[filename][num];

            let arr = this.state.list[filename];

            arr[num] = page_id;
            arr[ex_index] = ex_page;

            this.setState({
                list: {
                    ...this.state.list,
                    [filename]: arr
                }
            })
        }
    }


    /**
     * _handleDocTypeChange
     *
     * EVENT when change document type
     *
     * @param filename
     * @param page
     * @param e
     * @private
     */
    _handleDocTypeChange(filename, page, e) {
        this.props.changePageTypeHandler(e.target.value, filename, page);
    }

    /**
     * _handleFileClick
     *
     * EVENT on page preview click
     *
     * @param e
     * @param filename
     * @param page_id
     * @private
     */
    _handleFileClick(filename, page_id, e) {
        e.preventDefault();
        e.stopPropagation();

        this.setState({
            is_active: {
                filename: filename,
                page: page_id
            }
        })
    }


    /**
     * _handleFileCancel
     *
     * EVENT on cancel page click
     *
     * @param e
     * @param filename
     * @param page_id
     * @private
     */
    _handleFileCancel(filename, page_id, e) {
        e.preventDefault();
        e.stopPropagation();
    }


    /**
     * _checkIsPageActive
     *
     * Return true if current page in current file was checked
     *
     * @param filename {String} selected file
     * @param page_id {String} selected page
     * @returns {boolean}
     * @private
     */
    _checkIsPageActive(filename,page_id) {
        if (this.state.is_active == null) return false;
        if (this.state.is_active.filename in this.props.files) {
            if (this.state.is_active.page == page_id) return true;
            return false;
        }
        return false;
    }


    /**
     * _selectType
     *
     * Return Select element of document types
     *
     * @param filename
     * @param page_id
     * @returns {XML}
     * @private
     */
    _selectType(filename, page_id) {
        let options = Object.keys(DOC_TYPES).map(i => {
            return <option value={i} key={i}>{DOC_TYPES[i]}</option>
        });

        return (
            <select className="files-wrap__type" onChange={this._handleDocTypeChange.bind(this, filename, page_id)}>
                {options}
            </select>
        )
    }

    /**
     * _handleChangePage
     *
     * EVENT on change page number
     *
     * Here have to validate input value of page number
     *
     * @param filename
     * @param page_id
     * @param e
     * @private
     */
    _handleChangePage(filename, page_id, e) {
        if (e.target.value >= 1 && e.target.value <= Object.keys(this.props.files[filename].pages).length) {
            this.setPageNum(filename,page_id,e.target.value);
        } else if (e.target.value > Object.keys(this.props.files[filename].pages).length) {
            this.setPageNum(filename,page_id,1);
        } else {
            this.setPageNum(filename,page_id,Object.keys(this.props.files[filename].pages).length);
        }
    }


    /**
     * _pages
     *
     * Return list of pages
     *
     */
    _pages(pages, filename) {
        if (pages !== undefined && Object.keys(pages).length) {
            return Object.keys(pages).map((i,k) => {
                let url = pages[i];

                return (
                    <div className={classNames('file', 'file_preview', {'is-active': this._checkIsPageActive(filename,i)})}
                         key={k}
                         onClick={this._handleFileClick.bind(this, filename, i)} >
                        <div className="file__preview">
                            <button className="btn-cancel" type="button" onClick={this._handleFileCancel.bind(this, filename, i)}>
                                <i className="icon icon-cancel"></i>
                            </button>
                            <img src={url} alt={`${filename} / ${i}`}/>
                            <div className="file__extend">
                                <i className="icon icon-check"></i>< br/>
                                {::this._selectType(filename, i)}

                                {Object.keys(this.props.files[filename].pages).length > 1 ?
                                    <div className="form-group">
                                        <label>Страница:</label>
                                        <input type="number"
                                               className="form-control form-control_number"
                                               value={this.getPageNum(filename,i)}
                                               onChange={this._handleChangePage.bind(this,filename,i)}
                                        />
                                    </div>
                                    : null
                                }
                            </div>
                        </div>
                        <div className="file__name">{filename} / {this.getPageNum(filename,i)}</div>
                    </div>
                )
            });
        }
    }

    /**
     * _files
     *
     * Return all file pages for all files
     *
     */
    _files() {
        if (this.props.files !== undefined && Object.keys(this.props.files).length) {
            return Object.keys(this.props.files).map((i,k) => {
                let file = this.props.files[i];

                return (
                    <div  key={k}>
                        <div className="files-wrap__filename">{file.filename}</div>

                        {this._pages(file.pages, file.filename)}
                    </div>
                )
            })
        }
    }


    render() {
        return (
            <div className="files-wrap">
                <div className="files-wrap__img"><img src="./img/click.png" /></div>
                <div className="files-wrap__title">Определите, к каким документам относятся загруженные страницы.<br/>
                    <i style={{fontWeight: 'normal'}}>Для выбора типа документа кликайте на картинки из списка:</i>
                </div>

                {::this._files()}

            </div>
        )
    }
}
