/**
 * Created by alexboor on 3/2/2017.
 */
import 'babel-polyfill'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actions from '../actions/index'

import FilesWrap from './FilesWrap'
// import FilesGroups from './FilesGroups'
// import HelperPannel from './HelperPannel'
import FilesProgress from './FilesProgress'
import FilesUpload from './FilesUpload'

import Droparea from './droparea'

const classNames = require('classnames');


class Steps extends Component {

    constructor() {
        super();

        this.state = {
            // filesWrapIsActive: false,
            isInProgress: false
        }
    }

    componentDidUpdate() {
        console.debug(this.state);
    }

    _inProgress() {
        if (this.props.filesList !== undefined && Object.keys(this.props.filesList).length) {
            return Object.keys(this.props.filesList).map((i, k) => {
                return <FilesProgress key={k} file={this.props.filesList[i]} />
            });
        }

        return null;
    };


    
    render() {

        return (
            <div className="row">
                <div className="col-lg-12 col-md-12 text-center"><h3 id="step-current" data-count={this.props.total_steps}>{this.props.step}</h3></div>

                <div className="col-xs-12">
                    <div className="inner">
                        
                        <FilesUpload uploadHandler={this.props.actions.uploadFiles}/>

                        {/*<FilesGroups />*/}

                        {/*<HelperPannel />*/}

                        {this._inProgress()}


                        {/*<!-- BEGIN files-wrap -->*/}
                        <div className={classNames('files-wrap-overlay',{'is-active':this.props.isWrapActive})}></div>

                        { this.props.isWrapActive && Object.keys(this.props.filesList).length ?
                            <FilesWrap files={this.props.filesList} changePageTypeHandler={this.props.actions.changePageType}
                            />
                            : null
                        }


                    </div>
                </div>

                <div id="controls">
                    <button id="backBtn" type="button" className="btn btn-primary">Назад</button>
                    <button id="nextBtn" type="button" className="btn btn-primary">Вперед</button>
                </div>

                <Droparea uploadHandler={this.props.actions.uploadFiles}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    step: state.step,
    total_steps: state.total_steps,
    filesList: state.filesList,
    pages: state.pages,

    isWrapActive: state.isWrapActive
});


const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(actions, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(Steps)