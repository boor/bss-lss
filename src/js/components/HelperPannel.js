/**
 * Created by alexboor on 3/2/2017.
 */
import React, {Component} from 'react'

const classNames = require('classnames');


export default class HelperPannel extends Component {

    constructor() {
        super();

        this.state = {
        }
    }


    render() {

        return (
            <div className="helper-panel">
                <div className="row">
                    <div className="col-xs-3">
                        <div className="helper-panel__info">Выбрано 2 страницы</div>
                    </div>
                    <div className="col-xs-5">
                        <select className="form-control">
                            <option value="1">Паспорт</option>
                            <option value="2">ИНН</option>
                        </select>
                    </div>
                    <div className="col-xs-13">
                        <button id="okBtn" type="button" className="btn btn-primary">OK</button>
                    </div>
                    <div className="col-xs-14">
                        <button id="cancelBtn" type="button"  className="btn btn-primary">Отмена</button>
                    </div>

                    <div className="col-xs-4">
                        <div className="helper-panel__text">
                            DEL - удаление выбранных страниц <br />
                            SHIFT - выделение нескольких
                        </div>
                    </div>

                </div>
                <button className="btn-cancel" type="button">
                    <i className="icon icon-cancel"></i>
                </button>
            </div>
        )
    }
}



