/**
 * Created by alexboor on 3/2/2017.
 */

import 'babel-polyfill'
import React, {Component} from 'react'

export default class Buttons extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-lg-12 col-md-12 text-center" style={{marginBottom: '30px'}}><h3
                    id="step-current" data-count="0">0</h3></div>
                <div id="stepper">
                    <div className="step">
                        <form></form>
                    </div>
                    <div className="step">23</div>
                    <div className="step">34</div>
                </div>
                <div id="controls">
                    <button id="backBtn" type="button" className="btn btn-primary">Назад</button>
                    <button id="nextBtn" type="button" className="btn btn-primary">Вперед</button>
                </div>
            </div>
        )
    }
}

