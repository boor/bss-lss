/**
 * Created by alexboor on 3/2/2017.
 */

import 'babel-polyfill'
import React, {Component} from 'react'

const classNames = require('classnames');


export default class Droparea extends Component {

    constructor() {
        super();

        this.state = {
            isHidden: true
        }
    }

    componentDidMount() {

        document.addEventListener("dragover", e => {
            e.preventDefault();
            this.setState({isHidden: false});
        }, false);

        document.addEventListener("dragend", e => {
            this.setState({isHidden: true})
        }, false);

        document.addEventListener("drop", e => {
            e.preventDefault();
            this.setState({isHidden: true});

            this.props.uploadHandler(e.dataTransfer.files);
        }, false);
    }




    render() {

        return (
            <div className={classNames('droparea',{hidden:this.state.isHidden})}>

            </div>
        )
    }
}