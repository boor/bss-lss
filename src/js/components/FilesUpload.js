/**
 * Created by alexboor on 3/2/2017.
 */

import React, {Component} from 'react'

const classNames = require('classnames');


export default class FilesUpload extends Component {

    constructor() {
        super();

        this.state = {
        }
    }


    /**
     * _handleFileInputChange
     *
     * EVENT handle choose file from local disk
     *
     * @param e
     * @private
     */
    _handleFileInputChange(e) {
        e.preventDefault();
        this.props.uploadHandler(e.target.files);
    }


    /**
     * _handleAttachFileClick
     *
     * EVENT on click on Upload file button
     *
     * @private
     */
    _handleAttachFileClick(e) {
        e.preventDefault();
        this.refs.files.click();
    }

    render() {

        return (
            <div className="upload">
                <div className="upload__title">Загрузите документы, пожалуйста. </div>
                <p>
                    Поддерживаемые типы файлов: jpg, png, bmp, tif, pdf, doc, docx. <br />
                    <em>Перенесите файлы на эту страницу или нажмите кнопку:</em>
                </p>
                <div className="upload__row">
                    <div className="upload__col">
                        <img src="./img/dropzone.png" alt="я подсказка!" />
                    </div>
                    <div className="upload__col">
                        <div className="btn-file">
                            <button type="button" className="btn btn-primary" onClick={::this._handleAttachFileClick}>
                                <i className="icon icon-search"></i>
                                Обзор
                            </button>
                            <input type='file'
                                   name='input-file'
                                   ref='files'
                                   multiple
                                   onChange={::this._handleFileInputChange}
                                   hidden
                            />
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
