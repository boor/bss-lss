/**
 * Created by alexboor on 3/2/2017.
 */

import React, {Component} from 'react'

const classNames = require('classnames');


export default class FilesGroups extends Component {

    constructor() {
        super();

        this.state = {
        }
    }

    render() {

        return (
            <div className="panels-group">
                {/*<!-- BEGIN panel -->*/}
                <div className="panel is-danger">
                    <div className="panel__head">
                        <div className="panel__cell">1. Паспорт - основная страница (3-4) и страница с пропиской (5-6) </div>
                        <div className="panel__cell"><em>2 из 2 страниц</em></div>
                    </div>
                </div>
                {/*<!-- END panel -->*/}
                {/*<!-- BEGIN panel -->*/}
                <div className="panel is-success">
                    <div className="panel__head">
                        <div className="panel__cell">2. ИНН </div>
                        <div className="panel__cell"><em>1 из 1 страницы</em></div>
                    </div>
                </div>
                {/*<!-- END panel -->*/}
                {/*<!-- BEGIN panel -->*/}
                <div className="panel is-error">
                    <div className="panel__head">
                        <div className="panel__cell">3. СНИЛС</div>
                        <div className="panel__cell"><em>1 страница лишняя</em></div>
                    </div>
                </div>
                {/*<!-- END panel -->*/}
                {/*<!-- BEGIN panel -->*/}
                <div className="panel">
                    <div className="panel__head">
                        <div className="panel__cell">4. Заявление ЭЦП</div>
                        <div className="panel__cell"><em>0 из 1 страницы</em></div>
                    </div>
                </div>
                {/*<!-- END panel -->*/}
            </div>
        )
    }
}


