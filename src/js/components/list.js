/**
 * Created by alexboor on 3/2/2017.
 */

import 'babel-polyfill'
import React, {Component} from 'react'

export default class List extends Component {
    render() {
        return (
            <table className="text-center table table-bordered maintable">
                <tbody>
                <tr>
                    <th width="25%">Клиент</th>
                    <th width="8%">Время отправки</th>
                    <th width="18%">Документы на подписание</th>
                    <th width="21%">Статус</th>
                    <th>Загрузка документов</th>
                </tr>
                <tr>
                    <td>ИП 2 2 2</td>
                    <td>17:23:23 14.09.16</td>
                    <td>Документов на подписание нету</td>
                    <td>
                        <div className="fw-status status-awaiting-payment">Ожидание оплаты</div>
                        <br /><a style={{cursor: 'pointer'}}>Оплатить</a>
                    </td>
                    <td>
                        <button type="button" className="btn btn-primary">Загрузить документы</button>
                    </td>
                </tr>
                <tr>
                    <td>ИП 2 2 2</td>
                    <td>16:22:32 14.09.16</td>
                    <td>Документов на подписание нету</td>
                    <td>
                        <button disabled="" type="button" className="btn btn-primary btn-one">
                            Заявка не заполнена
                        </button>
                        <br /><a href="./register.php?edit=1290">Редактировать</a></td>
                    <td>
                        <button type="button" className="btn btn-primary" data-toggle="modal">Загрузить
                            документы
                        </button>
                    </td>
                </tr>
                <tr>
                    <td>ООО 2<br /><a type="button">Удалить заявку</a></td>
                    <td style={{fontSize:'14px'}}>21:42:45 18.08.16</td>
                    <td><p><a target="_blank" href="./comppath/vWEl65PcMG_reshooo.pdf">Решение ООО</a>
                    </p></td>
                    <td style={{fontSize: '14px'}}>
                        <button disabled="" type="button" className="btn btn-primary">Ожидание оплаты
                        </button>
                        <br /><a style={{cursor: 'pointer'}}>Оплатить</a></td>
                    <td>
                        <button type="button" className="btn btn-primary btn-two" data-toggle="modal" data-target="#or1091">
                            Загрузить документы
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        )
    }
}