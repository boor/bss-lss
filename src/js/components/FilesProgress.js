/**
 * Created by alexboor on 3/2/2017.
 */

import React, {Component} from 'react'

const classNames = require('classnames');


export default class FilesProgress extends Component {

    constructor() {
        super();

        this.state = {
        }
    }

    render() {

        if (this.props.file.show) {
            return (
                <div className="file-item file-item_progress">
                    <div className="row">
                        <div className="col-xs-4 col-xs-offset-2">
                            <div className="file">
                                <div className="file__cell">
                                    <div className="file__img">
                                        <img src="./img/file-loader.png" alt="file"/>

                                    </div>
                                </div>
                                <div className="file__cell">
                                    <div className="file__name">{this.props.file.filename}</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-3">
                            <div className="progress">
                                <progress max="100" value={this.props.file.progress || 0}></progress>
                                <span className="progress__value"
                                      style={{width: (this.props.file.progress || 0)+'%'}}>{this.props.file.progress || 0}</span>
                            </div>
                        </div>
                        <div className="col-xs-2">
                            {+this.props.file.progress == 100 && !this.props.file.error ?
                                <span className="progress-size">Обработка...</span>
                                : null
                            }
                        </div>
                    </div>
                    {this.props.file.error ?
                        <div className="row">
                            <div className="col-xs-16 text-center">
                                <span className="error-text">{this.props.file.error.message}</span>
                            </div>
                        </div>
                        : null
                    }

                </div>
            )
        }

        return <div></div>;
    }
}


