/**
 * Created by alexboor on 3/2/2017.
 */

import {
    UPLOAD_FILE_INIT,
    UPLOAD_FILE_SUCCESS,
    UPLOAD_FILE_FAIL,
    UPLOAD_FILE_PROGRESS,
    UPLOAD_FILE_CHANGE_PAGE
} from '../constants/index';


const initialState = {
    step: 1,
    total_steps: 3,
    filesList: {},
    types: {},
    pages: {},

    isWrapActive: false
};

export default function select(state = initialState, action) {

    switch (action.type) {

        case UPLOAD_FILE_INIT:
            return {
                ...state,
                filesList: {
                    ...state.filesList,
                    [action.payload.name]: {
                        'filename' : action.payload.name,
                        'file' : action.payload.file,
                        'progress' : action.payload.progress,
                        'show': true
                    }
                }
            };

        case UPLOAD_FILE_SUCCESS:
            return {
                ...state,
                isWrapActive: true,
                filesList: {
                    ...state.filesList,
                    [action.payload.name]: {
                        ...state.filesList[action.payload.name],
                        'success' : true,
                        'show': true,
                        'pages': action.payload.pages
                    }
                }
            };

        case UPLOAD_FILE_FAIL:
            return {
                ...state,
                isWrapActive: false,
                filesList: {
                    ...state.filesList,
                    [action.payload.name]: {
                        ...state.filesList[action.payload.name],
                        'error' : action.payload.error,
                        'show': true
                    }
                }
            };

        case UPLOAD_FILE_PROGRESS:
            return {
                ...state,
                filesList: {
                    ...state.filesList,
                    [action.payload.name]: {
                        ...state.filesList[action.payload.name],
                        'progress' : action.payload.progress,
                        'show': true
                    }
                }
            };

        case UPLOAD_FILE_CHANGE_PAGE:
            return {
                ...state,
                types: {
                    ...state.types,
                    [action.payload.name]: {
                        ...state.types[action.payload.name],
                        [action.payload.page]: action.payload.type
                    }
                }

            };

        default:
            return state;
    }
}