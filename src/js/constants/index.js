/**
 * Created by alexboor on 3/2/2017.
 */

/**
 * Constants for reducers
 *
 * @type {string}
 */

export const UPLOAD_FILE_INIT = 'UPLOAD_FILE_INIT';
export const UPLOAD_FILE_SUCCESS = 'UPLOAD_FILE_SUCCESS';
export const UPLOAD_FILE_FAIL = 'UPLOAD_FILE_FAIL';
export const UPLOAD_FILE_PROGRESS = 'UPLOAD_FILE_PROGRESS';

export const UPLOAD_FILE_CHANGE_PAGE = 'UPLOAD_FILE_CHANGE_PAGE';