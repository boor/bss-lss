/**
 * Created by alexboor on 3/2/2017.
 */


/**
 * Created by alexboor on 7/10/2016.
 */
'use strict';


import {
    UPLOAD_FILE_INIT,
    UPLOAD_FILE_SUCCESS,
    UPLOAD_FILE_FAIL,
    UPLOAD_FILE_PROGRESS,
    UPLOAD_FILE_CHANGE_PAGE
} from '../constants/index';

import FilesModel from '../models/files'

let filesModel = new FilesModel;

const FILE_SIZE_LIMIT = 20 * 1024 * 1024;

export function uploadFiles(files) {
    return (dispatch) => {
        for (var i = 0; i < files.length; i++) {
            let file = files[i];

            /**
             * Функция для коллбека, который отслеживает прогресс загрузки данных на сервер
             * Подробнее см. models/files
             *
             * @param e Object ProgressEvent объект
             */
            function onprogress(e) {
                if (e.lengthComputable) {
                    var progress = Math.ceil(e.loaded / e.total * 100);

                    dispatch({
                        type: UPLOAD_FILE_PROGRESS,
                        payload: {
                            name: file.name,
                            progress: (progress < 100) ? progress : 100
                        }
                    });
                }
            }

            if (file && file.type) {
                if (file.size > 0 && file.size <= FILE_SIZE_LIMIT) {

                    dispatch({
                        type: UPLOAD_FILE_INIT,
                        payload: {
                            name: file.name
                        }
                    });

                    var formData = new FormData();

                    formData.append('file', file);

                    /**
                     * Upload files to back-end. Check in models/files.js
                     */
                    filesModel.uploadFile(formData, onprogress)
                        .then(r => {
                            if ('order_id' in JSON.parse(r)) {
                                dispatch({
                                    type: UPLOAD_FILE_FAIL,
                                    payload: {
                                        name: file.name,
                                        error: Error('Не указан order_id')
                                    }
                                });
                            } else {
                                dispatch({
                                    type: UPLOAD_FILE_SUCCESS,
                                    payload: {
                                        name: file.name,
                                        pages: JSON.parse(r).pages,
                                        response: JSON.parse(r)
                                    }
                                });
                            }
                        })
                        .catch(err => {
                            console.error(err);
                            console.error(err);

                            dispatch({
                                type: UPLOAD_FILE_FAIL,
                                payload: {
                                    name: file.name,
                                    error: err
                                }
                            });
                        });

                } else if (file.size > fileSizeLimit) {
                    dispatch({
                        type: UPLOAD_FILE_FAIL,
                        payload: {
                            name: file.name,
                            error: new Error('Файл не может быть больше ' + ~~fileSizeLimit / 1024 / 1024 + ' Мб')
                        }
                    });
                }
            }

        }
    }; // return
} // function


export function changePageType(type, filename, page) {
    if (type != 0) {
        return (dispatch) => {
            dispatch({
                type: UPLOAD_FILE_CHANGE_PAGE,
                payload: {
                    type: type,
                    name: filename,
                    page: page
                }
            })

        }
    }
}