/**
 * Created by alexboor on 1/2/2017.
 */

import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'


// import Buttons from './components/buttons'
// import List from './components/list'


import Steps from './components/Steps'


import configureStore from './store/configureStore'

const store = configureStore();

render(
    <Provider store={store}>
        <div>
            <Steps />
        </div>
    </Provider>,
    document.getElementById('js-root')
);