/**
 * Created by alexboor on 3/2/2017.
 */

const UPLOAD_URL = 'http://api.bss-llc.ru/document/upload/?access-token=6umybNm4l8fgTe6FVSwp5vSpY5AjkbHI';

export default class FilesModel {


    /**
     * uploadFile
     *
     * Загрузка файла на сервер
     *
     * Так как мы на взод принимаем уже готовый объект FormData, то мы нет необходимости принудительно
     * устанавливать Content-Type в multipart/form-data. По спецификации это должно происходить автоматически.
     * На браузерах, что есть у меня в наличии это так и происходит.
     *
     * @param url        String   URL куда будем бытаться отправить файл
     * @param formdata   Object   Объект FormData с данными для загрузки.
     * @param onProgress Function Коллбек, который будет дергаться при загрузки файла, именно там надо отслеживать
     *                            прогресс. Колбек получает единственный параметр на вход с объектом ProgressEvent
     *
     * @returns {Promise}
     */
    uploadFile(formdata, onProgress) {

        return new Promise((resolve, reject) => {

            formdata.append('order_id','2');

            /**
             * Это работает, но fetch на данный момент не имеет коллбека для отслеживания процесса
             * Поэтому я просто оставлю это здесь, когда-нибудь, возможно, это понадобится.
             * Ниже реализация того же самого, но только через XHR
             * */
                // fetch(url, {
                //     method: 'POST',
                //     credentials: 'include',
                //     headers: {
                //         'X-CSRFToken' : this._getCsrfToken()
                //     },
                //     body: formdata
                // })
            var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;

            var xhr = new XHR();

            xhr.open('POST', UPLOAD_URL, true);

            xhr.onload = e => resolve(e.target.responseText);
            xhr.onerror = reject;

            if (xhr.upload && onProgress)
                xhr.upload.onprogress = onProgress;

            xhr.send(formdata);

        });
    }


}