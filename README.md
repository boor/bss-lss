# Hi there

### How to launch  

- in console `git clone REPO_ADDRESS`
- go to project folder 
- open `upload_new.html` in browser


### How to build

- `git clone REPO_ADDRESS`
- go to project folder
- `npm install`
- `npm run build`
- profit


### Test 

Not implemented yet


### Deploy

Not CI ready yet