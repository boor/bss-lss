var path = require('path');
var webpack = require('webpack');
// var autoprefixer = require('autoprefixer');
// var precss = require('precss');

var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    devtool: 'cheap-module-eval-source-map',
    entry: [
        'babel-polyfill',
        './src/js',
        './src/styles/app.less'
    ],
    output: {
        path: path.join(__dirname, './js'),
        filename: 'app.js'
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new ExtractTextPlugin("../css/app.css"),
        // new webpack.HotModuleReplacementPlugin()
    ],
    module: {
        loaders: [
            {
                loaders: ['babel-loader'],
                include: [
                    path.resolve(__dirname, "./src/js")
                ],
                test: /\.js$/,
                plugins: ['transform-runtime']
            },

            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader")
                // loader: "style-loader!css-loader!postcss-loader"
            },
            {
                test: /\.less$/,
                // loader: "style-loader!css-loader!postcss-loader"
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
            }
        ]
    }
};